from __future__ import annotations

import os, tempfile, tomllib
from datetime import datetime
from pathlib import Path
from typing import Any, Optional, Sequence, Union

import jinja2

from hidos import Succession, Edition
from hidos.cache import SuccessionCache
from hidos.dsi import BaseDsi, EditionId

from epijats import Eprint, EprinterConfig, Webstract
from epijats.jinja import style_template_loader
from epijats.webstract import WebstractFacade


class WebPageGenerator:
    def __init__(self, dest_dir_path: Path, loaders: Sequence[jinja2.BaseLoader]):
        self.dest = dest_dir_path
        self.env = jinja2.Environment(
            loader=jinja2.ChoiceLoader(loaders),
            trim_blocks=True,
            lstrip_blocks=True,
            keep_trailing_newline=True,
            extensions=["jinja2.ext.do"],
        )

    def gen_file(
        self,
        tmpl_subpath: Union[str, Path],
        dest_subpath: Union[str, Path],
        ctx: dict[Any, Any] = dict()
    ) -> None:
        dest = self.dest / dest_subpath
        os.makedirs(dest.parent, exist_ok=True)
        ctx = dict(ctx, this=self.site_ctx(dest_subpath))
        tmpl = self.env.get_template(str(tmpl_subpath))
        tmpl.stream(**ctx).dump(str(dest), "utf-8")

    def site_ctx(self, subpath: Union[str, Path]) -> dict[str, str]:
        depth = len(Path(subpath).parents) - 1
        ret = {
            "relroot": "../" * depth if depth > 0 else "./",
        }
        return ret


class BaseprintPageGenerator(WebPageGenerator):
    def __init__(self, dest_dir_path: Path):
        super().__init__(dest_dir_path, [
            style_template_loader(),
            jinja2.PrefixLoader(
                {"baseprintpress": jinja2.PackageLoader(__name__, "templates")}
            ),
        ])


class Config:
    def __init__(self, config_file: Path):
        with open(config_file, "rb") as f:
            data = tomllib.load(f)
        self.successions = data.get("successions", dict())
        self.site_dir = Path(data.get("site_dir", "site"))


class SuccessionFacade:
    def __init__(self, succession: Succession, editions: BaseprintEditions):
        self.succession = succession
        self._editions = []
        for e in succession.all_editions():
            if e.snapshot:
                self._editions.append(editions.edition_facade(e))

    @property
    def dsi(self) -> str:
        return str(self.succession.dsi)

    @property
    def revision(self) -> str:
        return self.succession.revision

    @property
    def editions(self):
        return self._editions


class BaseprintEditions:
    def __init__(
        self,
        succession: Succession,
        tmpdir: Path,
        dates: dict[EditionId, datetime | None]
    ):
        self._webstracts = dict()
        self._tmpdir = tmpdir
        for e in succession.root.all_subeditions():
            if e.snapshot:
                ws = Webstract.from_edition(e, tmpdir / str(e.dsi))
                archive_date = dates.get(e.edid)
                if archive_date:
                    ws["archive_date"] = archive_date.date()
                self._webstracts[e.edid] = ws

    def edition_facade(self, e: Edition) -> Optional[WebstractFacade]:
        flow = e.flow_edition()
        return self._webstracts[flow.edid].facade if flow else None

    def make_pdf(self, e: Edition, pdf_dest):
        config = EprinterConfig(dsi_domain="perm.pub")
        if not pdf_dest.exists():
            print(pdf_dest)
            eprint = Eprint(self._webstracts[e.edid], self._tmpdir, config)
            eprint.make_pdf(pdf_dest)


def render_succession(
    gen: WebPageGenerator,
    succession: Succession,
    dates: dict[EditionId, datetime | None],
) -> None:
    with tempfile.TemporaryDirectory() as tmpdir:
        baseprints = BaseprintEditions(succession, Path(tmpdir), dates)
        facade = SuccessionFacade(succession, baseprints)
        for e in succession.all_editions():
            ctx = dict(
                succession=facade,
                path_edid=e.edid,
                edition=baseprints.edition_facade(e),
            )
            subpath = Path(str(e.dsi))
            gen.gen_file("baseprintpress/edition.html.jinja", subpath / "index.html", ctx)
            if e.snapshot:
                baseprints.make_pdf(e, gen.dest / subpath / "article.pdf")
        gen.gen_file(
            "baseprintpress/succession_info.html.jinja",
            Path(str(succession.dsi)) / "about" / "index.html",
            dict(succession=facade),
        )


def make_website(config_file: Path, *, offline: bool) -> None:
    config = Config(config_file)
    cache = SuccessionCache(offline=offline)
    gen = BaseprintPageGenerator(config.site_dir)
    Eprint.copy_static_dir(config.site_dir / "static")
    for key in config.successions.keys():
        dsi = BaseDsi(key)
        print(dsi)
        ds = cache.get(dsi)
        dates = cache.archive_dates(ds)
        render_succession(gen, ds, dates)
    ctx = dict(dsis=config.successions.keys())
    gen.gen_file("baseprintpress/home.html.jinja", "index.html", ctx)
