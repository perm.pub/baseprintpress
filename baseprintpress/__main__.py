import logging
from sys import stderr
from argparse import ArgumentParser
from pathlib import Path
from typing import Any

from hidos.util import LOG

from .press import make_website


def main(args: Any = None) -> int:
    LOG.setLevel(logging.INFO)
    LOG.addHandler(logging.StreamHandler())

    parser = ArgumentParser(prog="baseprintpress")
    parser.add_argument(
        "-f",
        "--config-file",
        type=Path,
        help="Config file",
        default="baseprintpress.toml",
    )
    parser.add_argument(
        "--offline", action="store_true", help="Make offline, use cache only"
    )
    args = parser.parse_args()
    if not args.config_file.exists():
        print(f"Configuration file not found: {args.config_file}", file=stderr)
        return 1
    make_website(args.config_file, offline=args.offline)
    return 0


if __name__ == "__main__":
    exit(main())
